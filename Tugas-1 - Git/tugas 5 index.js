//SOAL 1

var daftarHewan = ["2. komodo", "5. buaya", "3. cicak", "4. ular", "1. tokek"];
for (var i = 1; i < daftarHewan.length; i++){
    for (var j = 0; j < i; j++){
        if (daftarHewan[i] < daftarHewan[j]) {
          var x = daftarHewan[i];
          daftarHewan[i] = daftarHewan[j];
          daftarHewan[j] = x;
        }
    }
}
console.log('Berikut ini adalah contoh daftar hewan reptil :');
for (var e = 0; e < daftarHewan.length; e++) console.log(daftarHewan[e]);
console.log('\n')

//SOAL 2

function introduce(data){
    return 'Nama saya ' + data['name'] + ', umur saya ' + data['age'] + ' tahun, alamat saya di ' + data['address'] + ', dan hobby saya ' + data['hobby'] + '!'
}
 
var data = {name : "raihan purnama" , age : 20 , address : "Jalan cihanjuang-jati cimahi utara" , hobby : "berolahraga" }
 
var perkenalan = introduce(data)
console.log('data perkenalan: \n', perkenalan, '\n') 

//SOAL 3

function hitung_huruf_vokal(dataVokal){
    var count = 0;
    var hurufVokal = dataVokal.toLowerCase().split('');
    for(var i = 0; i < hurufVokal.length; i++){
        if(hurufVokal[i] == 'a' || hurufVokal[i] == 'i' || hurufVokal[i] == 'u' || hurufVokal[i] == 'e' || hurufVokal[i] == 'o'){
            count += 1;
        }
    }
    return count;
}

var hurufVokal1 = hitung_huruf_vokal("Raihan") 
var hurufVokal2 = hitung_huruf_vokal("Purnama") 
var hurufVokal3 = hitung_huruf_vokal("Putra") 
var hurufVokal4 = hitung_huruf_vokal("Zena") 
console.log('Huruf Vokal Pada Nama Saya Ada : \n ', hurufVokal1 , hurufVokal2, hurufVokal3, hurufVokal4, '\n Pada Masing - Masing Setiap Kata')

// soal no 4

function deretAritmatika(n){
    var a = -2;
    var b = 2;
    n += 1;
    result = a + (n-1) * b;
    return result;
}
console.log('')
console.log('Deret Aritmatika n=2 :');
console.log('2 = ', deretAritmatika(2))
console.log('3 = ', deretAritmatika(3))
console.log('4 = ', deretAritmatika(4))
console.log('5 = ', deretAritmatika(5))
console.log('7 = ', deretAritmatika(7))
console.log('8 = ', deretAritmatika(8))
