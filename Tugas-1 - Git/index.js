//SOAL 1

var nilai = 95;

if(nilai >= 85) indeksNilai = "A"
else if(nilai >= 75) indeksNilai = "B"
else if(nilai >= 65) indeksNilai = "C"
else if(nilai >= 55) indeksNilai = "D"
else indeksNilai = "E";

console.log(`Indeks Nilai Anda Adalah : ${indeksNilai}`);

//SOAL 2

var tanggal = 09;
var bulan = 3;
var tahun = 2001;

switch(bulan) {
	case 1: bulan = "Januari"; break;
	case 2: bulan = "Februari"; break;
	case 3: bulan = "Maret"; break;
	case 4: bulan = "April"; break;
	case 5: bulan = "Mei"; break;
	case 6: bulan = "Juni"; break;
	case 7: bulan = "Juli"; break;
	case 8: bulan = "Agustus"; break;
	case 9: bulan = "September"; break;
	case 10: bulan = "Oktober"; break;
	case 11: bulan = "November"; break;
	case 12: bulan = "Desember"; break;
}
var raihanBirthday = "dunia menyambut kelahiran raihan pada : " + tanggal + ", " + bulan + " " + tahun;
console.log(raihanBirthday);

//SOAL 3

console.log('\n');
var s = '';
var pola = 7;

for (var i = 1; i <= pola; i++) {
	for (var j = 1; j <= i; j++) {
		s += '#';
	}
	s += '\n';
}

console.log(s);

//SOAL 4
var m = 10;
var s = 1;
var hasil = '';
var pembatas = '';

for(var i=1; i <= m; i++){
    if(s > 3){
        s = 1;
    }

    switch(s){
        case 1: hasil += i + ' - I Love Programming \n'; break;
        case 2: hasil += i + ' - I Love JavaScript \n'; break;
        case 3: pembatas += '==='; hasil += i + ' - I Love Vue.js \n ' + pembatas + '\n'; break;
    }

    s++;
}

console.log('\n' + hasil + '\n');