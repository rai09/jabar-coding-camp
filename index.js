//SOAL 1

var first = "saya sangat senang hari ini" 
var second = "belajar javascript itu keren" 

var sliceOne = first.slice(0,4);
var sliceTwo = first.slice(12,18);
var sliceThree = second.slice(0,7);
var sliceFour = second.slice(8,18);
var sliceFourUpper = sliceFour.toUpperCase();
console.log(sliceOne + " " + sliceTwo + " " + sliceThree + " " + sliceFourUpper);

//SOAL 2

var a = "10";
var b = "2";
var c = "4";
var d = '6';

hasil = Number(a) * Number(b) + Number(c);
console.log(hasil);

//SOAL 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14); 
var kataKetiga = kalimat.substring(15,18); 
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima); 